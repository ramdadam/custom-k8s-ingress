package custom_ingress_controller

import (
	"fmt"
	"log"
	"net/http/httputil"
	"net/url"
)

type Proxy struct {
	backend string
	proxy   *httputil.ReverseProxy
}

func NewProxyWithBackend(backend string) (*Proxy, error) {
	uri, err := url.ParseRequestURI(backend)
	if err != nil {
		log.Println(fmt.Sprintf("backend is invalid: %s. error was: %s", backend, err.Error()))
		return nil, err
	}
	reverseProxy := &Proxy{backend: backend, proxy: httputil.NewSingleHostReverseProxy(uri)}
	return reverseProxy, nil
}
