package custom_ingress_controller

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestProxy(t *testing.T) {
	t.Run("invalid backend throws", func(t *testing.T) {
		proxy, err := NewProxyWithBackend("")
		assert.Nil(t, proxy)
		assert.Error(t, err)
	})
	t.Run("valid backend returns a proxy", func(t *testing.T) {
		proxy, err := NewProxyWithBackend("https://www.google.com")
		assert.Nil(t, err)
		assert.NotNil(t, proxy)
		assert.Equal(t, "https://www.google.com", proxy.backend)
	})
}
