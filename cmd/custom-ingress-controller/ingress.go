package main

import (
	base "custom-ingress-controller"
	"fmt"
	"log"
	"os"
)
import networkingv1 "k8s.io/api/networking/v1"

func main() {
	log.SetOutput(os.Stdout)
	server := base.NewIngressServer("0.0.0.0", "80")
	onChange := func(ingress *networkingv1.Ingress) {
		log.Println(fmt.Sprintf("on Change called for %s ingress", ingress.Name))
		log.Println(fmt.Sprintf("rule: %s", ingress.Spec.Rules[0].IngressRuleValue.String()))
		log.Println(fmt.Sprintf("rule: %s", ingress.Spec.Rules[0].IngressRuleValue.HTTP.Paths[0].Path))
		for _, backend := range ingress.Spec.Rules[0].IngressRuleValue.HTTP.Paths {
			service := backend.Backend.Service
			err := server.AddProxy(backend.Path, fmt.Sprintf("http://%s:%d", service.Name, service.Port.Number))
			if err != nil {
				log.Println(fmt.Sprintf("failed to add backend (%s, %d) to proxy", service.Name, service.Port.Number))
			}
			log.Println(fmt.Sprintf("added path (%s) to proxy to backend (%s:%d)", backend.Path, service.Name, service.Port.Number))
		}
		err := server.StartServer()
		if err != nil {
			log.Println(fmt.Sprintf("failed to start server: %s", err))
			panic(err)
		}
	}
	log.Println("starting custom-ingress-controller")
	_watcher := &base.Watcher{}
	_watcher.Init(onChange)
	stopCh := make(chan struct{})
	_watcher.RunIngressInformer(stopCh)
	select {}
}
