# custom-ingress-controller
A basic (and insecure) custom ingress controller 
## Description
It supports the very basic idea of the Kubernetes Ingress API: proxying requests. 

**DON'T USE IT FOR ANYTHING BUT LEARNING**

## Motivation
As part of my research of the Kubernetes Gateway API I wanted to understand the "behind the scenes" of the Ingress API.
I have generally a hands-on mentality, that's why I decided to build an ingress controller to understand it.

I pushed the code to help others understand it too, if they/you want.

## Acknowledgments

The [ingress-nginx](https://github.com/kubernetes/ingress-nginx) project was a big help for me to get "the bigger picture", so thanks!