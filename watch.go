package custom_ingress_controller

import (
	networkingv1 "k8s.io/api/networking/v1"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/listers/networking/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
)

type onChange func(ingress *networkingv1.Ingress)

type Watcher struct {
	ingress          cache.SharedIndexInformer
	lister           v1.IngressLister
	onChangeCallback onChange
}

func (w *Watcher) Init(onChangeCallback onChange) {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	handleIngressChanges := func(obj interface{}) {
		ingress, ok := toIngress(obj)
		if ok && w.onChangeCallback != nil {
			for _, rule := range ingress.Spec.Rules {
				print(rule.Host)
			}
			w.onChangeCallback(ingress)
		}
	}
	w.onChangeCallback = onChangeCallback
	informerFactory := informers.NewSharedInformerFactory(clientset, 0)
	w.ingress = informerFactory.Networking().V1().Ingresses().Informer()
	w.ingress.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			handleIngressChanges(obj)
		},
		UpdateFunc: func(oldObj interface{}, obj interface{}) {
			handleIngressChanges(obj)
		},
		DeleteFunc: func(obj interface{}) {
			handleIngressChanges(obj)
		},
	})
	w.lister = informerFactory.Networking().V1().Ingresses().Lister()
}

func (w *Watcher) RunIngressInformer(stopCh <-chan struct{}) {
	go w.ingress.Run(stopCh)
}

// "inspired" by:
// https://github.com/kubernetes/ingress-nginx/blob/3579ed04870c77979ec5bf18f4cd00c8763615a1/internal/ingress/controller/store/store.go#L1142
func toIngress(obj interface{}) (*networkingv1.Ingress, bool) {
	if ing, ok := obj.(*networkingv1.Ingress); ok {
		return ing, true
	}
	return nil, false
}
