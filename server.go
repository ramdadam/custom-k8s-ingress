package custom_ingress_controller

import (
	"errors"
	"fmt"
	"log"
	"net/http"
)

type IngressServer struct {
	server  *http.Server
	proxies []*Proxy
	mux     *http.ServeMux
}

func NewIngressServer(host string, port string) *IngressServer {
	ingressServer := &IngressServer{}
	ingressServer.mux = http.NewServeMux()
	ingressServer.server = &http.Server{Addr: fmt.Sprintf("%s:%s", host, port), Handler: ingressServer.mux}
	return ingressServer
}

func (i *IngressServer) AddProxy(path string, backendHost string) error {
	proxy, err := NewProxyWithBackend(backendHost)
	if err != nil {
		log.Println(fmt.Sprintf("failed to create a proxy for backend %s", backendHost))
		return err
	}
	i.proxies = append(i.proxies, proxy)
	i.mux.Handle(path, proxy.proxy)
	return nil
}

func (i *IngressServer) StartServer() error {
	if i.server == nil {
		return errors.New("server not initialized")
	}
	err := i.server.ListenAndServe()
	return err
}
